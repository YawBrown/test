from typing import Optional
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

let g:ale_linters={'python':['flake8']}
let g:ale_fixers={'*':[],'python':[black]}

let g:ale_fix_on_save=1


class Item(BaseModel):
    id: int
    name: str
    weight: float
    description: Optional[str] = None

Items =  [
        {
            "id": 0,
            "name": "Soap",
            "weight": 1.5,
            "description": "Geisha Soap"
        }
    ]


@app.get("/items")
async def getAllItems():
    return Items

@app.get("/items/{Iid}")
async def getUser(Iid: int):
    new = [u for u in Items if u['id'] == Iid]
    return new[0] if len(new) > 0 else {}

@app.post("/create")
async def addItem(item: Item):
    new_item = {
        "id": item.id,
        "name": item.name,
        "weight": item.weight,
        "description": item.description
    }
    Items.append(new_item)
    return Items

@app.put("/update/{id}")
async def updateUser(item: Item, id: int):
    new_details = {
        "id": id,
        "name": item.name,
        "weight": item.weight,
        "description": item.description
    }

    search_person = [u for u in Items if u['id'] == id]
    if len(search_person) > 0:
        Items.remove(search_person[0])
        Items.append(new_details)
    return Items
